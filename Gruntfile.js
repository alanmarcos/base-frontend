'use strict';

var path = '.';
module.exports = function(grunt) {

	require('time-grunt')(grunt);

	// Project configuration.
	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),
		path: path,

		watch: {
			gruntfile: {
				files: ['Gruntfile.js'],
				options: {
					reload: true
				}
			},
			less: {
				files: ['<%= path %>/less/**/*.less'],
				tasks: ['less:dev', 'postcss:dev']
			},
			css: {
				files: ['<%= path %>/css/**/*.css'],
				options: {
					livereload: true,
				}
			},
			phpFiles: {
				files: ['<%= path %>/**/*.php'],
				options: {
					livereload: true
				}
			}
		},
		less: {
			dev: {
				files: {
					'<%= path %>/css/style.min.css' : '<%= path %>/less/main.less'
				}
			}
		},
		postcss: {
			options: {
				map: false,
				processors: [
					require('autoprefixer')({browsers: 'last 3 versions'})
				]
			},
			dev: {
				src: '<%= path %>/css/style.min.css'
			}
		},		
		githooks: {
			all: {
				'post-merge': {
					taskNames: 'setup'
				}
			}
		},
		exec: {
  			setup: 'npm update && bower install'
		},
		combine_mq: {
			combine: {
				expand: true,
				cwd: '<%= path %>/css',
				src: '*.min.css',
				dest: '<%= path %>/css/'
			}
		},	
		modernizr: {
			dev: {
				devFile: '<%= path %>/bower_components/modernizr/modernizr.js',
				outputFile: '<%= path %>/js/modernizr.custom.min.js',
				"extra": {
					shiv: false
				},
				"parseFiles" : true,
				"files" : {
					"src": [
						'<%= path %>/less/*.less',
						'<%= path %>/js/*.js'
					]
				}
			}
		},
		useminPrepare: {
			html: '<%= path %>/*.php',
			options: {
				dest: '<%= path %>/'
			}
		},
		usemin: {
			html: '<%= path %>/index.php'
		},
		cacheBust: {
			options: {
				encoding: 'utf8',
				length: 16
			},
			scripts: {
				files: [{
					src: '<%= path %>/*.php'
				}]
			}
		}
	});

	//load tasks
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-githooks');
	grunt.loadNpmTasks('grunt-postcss');

	//register tasks
	grunt.registerTask('default', [], function(){

		grunt.loadNpmTasks('grunt-modernizr');
		grunt.loadNpmTasks('grunt-contrib-watch');
		grunt.task.run('less', 'postcss', 'modernizr', 'watch');

	});

	grunt.registerTask('setup', [], function(){

		grunt.loadNpmTasks('grunt-exec');
		grunt.loadNpmTasks('grunt-modernizr');
		grunt.task.run('exec', 'less', 'postcss', 'modernizr');

	});

	grunt.registerTask('build', [], function(){

		grunt.loadNpmTasks('grunt-usemin');	
		grunt.loadNpmTasks('grunt-contrib-concat');
		grunt.loadNpmTasks('grunt-contrib-cssmin');
		grunt.loadNpmTasks('grunt-combine-mq');	
		grunt.loadNpmTasks('grunt-contrib-uglify');
		grunt.loadNpmTasks('grunt-modernizr');
		grunt.loadNpmTasks('grunt-cache-bust');

		grunt.task.run('less', 'postcss', 'useminPrepare', 'concat', 'cssmin', 'combine_mq', 'uglify', 'modernizr', 'usemin', 'cacheBust');

	});

};
