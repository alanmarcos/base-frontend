<!DOCTYPE html>
<html class="no-js">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Base</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">

	<!-- build:css css/style.min.css -->
	<link rel="stylesheet" type="text/css" href="css/style.min.css">
	<!-- endbuild -->
	
	<!-- build:js js/iefallbacks.min.js -->
	<!--[if lt IE 9]>
	<script type="text/javascript" src="bower_components/respond/src/respond.js"></script>
	<script type="text/javascript" src="bower_components/html5shiv/dist/html5shiv.js"></script>
	<![endif]-->
	<!-- endbuild -->	

	<script type="text/javascript" src="js/modernizr.custom.min.js"></script>

</head>
<body>



	
	<!-- build:js js/jquery.legacy.min.js -->
	<!--[if lt IE 9]>
	<script type="text/javascript" src="bower_components/jquery-legacy/dist/jquery.js"></script>
	<![endif]-->
	<!-- endbuild -->
	
	<!-- build:js js/jquery.min.js -->
	<!--[if gte IE 9]><!-->
	<script type="text/javascript" src="bower_components/jquery-modern/dist/jquery.js"></script>
	<!--<![endif]-->
	<!-- endbuild -->	

	<!-- build:js js/plugins.min.js -->
	<script type="text/javascript" src="js/scripts.js"></script>
	<!-- endbuild -->

	<script type="text/javascript">
	var _gas = _gas || [];
	_gas.push(['_setAccount', 'UA-YYYYYY-Y']); // REPLACE WITH YOUR GA NUMBER
	_gas.push(['_setDomainName', '.mydomain.com']); // REPLACE WITH YOUR DOMAIN
	_gas.push(['_trackPageview']);
	_gas.push(['_gasTrackForms']);
	_gas.push(['_gasTrackOutboundLinks']);
	_gas.push(['_gasTrackMaxScroll']);
	_gas.push(['_gasTrackDownloads']);
	_gas.push(['_gasTrackVideo']);
	_gas.push(['_gasTrackAudio']);
	_gas.push(['_gasTrackYoutube', {force: true}]);
	_gas.push(['_gasTrackVimeo', {force: true}]);
	_gas.push(['_gasTrackMailto']);

	(function() {
	var ga = document.createElement('script');
	ga.id = 'gas-script';
	ga.setAttribute('data-use-dcjs', 'false');
	ga.type = 'text/javascript';
	ga.async = true;
	ga.src = '//cdnjs.cloudflare.com/ajax/libs/gas/1.11.0/gas.min.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);
	})();
	</script>

	<!--[if lt IE 8]>
	<script type="text/javascript">
	var $buoop = {};
	$buoop.ol = window.onload;
	window.onload=function(){
	 try {if ($buoop.ol) $buoop.ol();}catch (e) {}
	 var e = document.createElement("script");
	 e.setAttribute("type", "text/javascript");
	 e.setAttribute("src", "//browser-update.org/update.js");
	 document.body.appendChild(e);
	}
	</script>
	<![endif]-->

</body>
</html>